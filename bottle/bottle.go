package bottle

// problem b

import (
	"fmt"
	"math"
)

type Prism struct {
	vector    []float64
	low, high float64
}

func New(a []float64, x0, x1 float64) *Prism {
	p := new(Prism)
	p.vector = initVector(a)
	p.low = x0
	p.high = x1
	return p
}

func (id *Prism) Volume() float64 {
	return volume(id.vector, id.low, id.high)
}

func (id *Prism) Distances(v float64) ([]float64, error) {
	v0 := id.Volume()
	if v > v0 {
		return nil, fmt.Errorf("insufficient volume")
	}
	n := int(v0 / v)
	if n > 8 {
		n = 8
	}
	ds := make([]float64, n)
	low := id.low
	for i := 0; i < n; i++ {
		k := float64(0.0001)
		for volume(id.vector, low, low+k) < v {
			k += 0.0001
		}
		low += k
		ds[i] = low - id.low
	}
	return ds, nil
}

func initVector(a []float64) []float64 {
	m := len(a) // 0 ~ n
	matrix := make([][]float64, m)
	for i := 0; i < m; i++ {
		matrix[i] = make([]float64, m)
		for j := 0; j < m; j++ {
			matrix[i][j] = a[i] * a[j]
		}
	}
	vec := make([]float64, 2*m-1)
	for i := 0; i < 2*m-1; i++ {
		vec[i] = 0
		k := i - m + 1
		if k < 0 {
			k = 0
		}
		j := i - k
		h := j
		for k <= h {
			vec[i] += matrix[k][j]
			k++
			j--
		}
	}
	return vec
}

func volume(vec []float64, x0, x1 float64) float64 {
	m := len(vec) // 0 ~ 2n+1
	r := float64(0)
	var c float64
	for i := 0; i < m; i++ {
		c = float64(i + 1)
		r += vec[i] * (math.Pow(x1, c) - math.Pow(x0, c)) / c
	}
	return r * math.Pi
}
