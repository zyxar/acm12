package bottle

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
)

var _vol_ []string
var _dis_ []string

func init() {
	_vol_ = make([]string, 4)
	_dis_ = make([]string, 4)
	_vol_[0] = "263.89"
	_vol_[1] = "263.89"
	_vol_[2] = "50.00"
	_vol_[3] = "31.42"
	_dis_[0] = "0.51 1.06 1.66 2.31 3.02 3.83 4.75 5.87"
	_dis_[1] = "insufficient volume"
	_dis_[2] = "2.00 4.00"
	_dis_[3] = "3.18 6.37 9.55"
}

func TestVolume(t *testing.T) {
	file, err := os.Open("sample.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	var line, out string
	var p *Prism
	var a []float64
	var b []string
	var x0, x1, inc float64
	i := 0
	for {
		_, err = reader.ReadString('\n')
		if err != nil {
			break
		}
		line, _ = reader.ReadString('\n')
		b = strings.Fields(line)
		a = make([]float64, len(b))
		for i := 0; i < len(b); i++ {
			a[i], _ = strconv.ParseFloat(b[i], 64)
		}
		line, _ = reader.ReadString('\n')
		b = strings.Fields(line)
		x0, _ = strconv.ParseFloat(b[0], 64)
		x1, _ = strconv.ParseFloat(b[1], 64)
		inc, _ = strconv.ParseFloat(b[2], 64)
		p = New(a, x0, x1)
		out = fmt.Sprintf("%.2f", p.Volume())
		if out != _vol_[i] {
			t.Errorf("Volume mismatch: got \"%v\", expected \"%v\"\n", out, _vol_[i])
		}
		fmt.Printf("Case %d: %s\n", i, out)
		ds, err := p.Distances(inc)
		if err != nil {
			out = fmt.Sprintf("%s", err)
		} else {
			out = fmt.Sprintf("%.2f", ds)
			out = out[1 : len(out)-1]
		}
		if out != _dis_[i] {
			t.Errorf("Distance mismatch: got \"%v\", expected \"%v\"\n", out, _dis_[i])
		}
		fmt.Println(out)
		i++
	}
	return
}
