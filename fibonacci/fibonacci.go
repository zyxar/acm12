package main

import (
	"fmt"
)

var lm []int64

func init() {
	lm = make([]int64, 100)
	lm[0] = 1
	lm[1] = 1
	for i := 2; i < 100; i++ {
		lm[i] = lm[i-1] + lm[i-2]
	}
}

func f(n int) string {
	switch n {
	case 0:
		return "0"
	case 1:
		return "1"
	}
	return f(n-1) + f(n-2)
}

func getfib(n int) int {
	m := 0
	for ; m < 100; m++ {
		if lm[m] >= int64(n) {
			break
		}
	}
	return m
}

func count(src []byte, pattern string) int64 {
	ret := int64(0)
	l := len(pattern)
	k := len(src) - l
	for i := 0; i <= k; i++ {
		if string(src[i:i+l]) == pattern {
			ret++
		}
	}
	return ret
}

func calcmid(pattern string) (int64, int64) {
	l := len(pattern)
	m := getfib(l)
	b1 := []byte(f(m))
	b2 := []byte(f(m + 1))
	b1 = append(b1[len(b1)-l+1:], b1[:l-1]...)
	b2 = append(b2[len(b2)-l+1:], b2[:l-1]...)
	return count(b1, pattern), count(b2, pattern)
}

func calcend(pattern string) (int64, int64, int) {
	l := len(pattern)
	lim := getfib(l)
	fib1 := f(lim)
	fib2 := f(lim + 1)
	b1 := []byte(fib1)
	b2 := []byte(fib2)
	return count(b2, pattern), count(b1, pattern), lim
}

func brute(num int, pattern string) int64 {
	if num > 20 { // suitable when n is small enough
		return -1
	}
	b := []byte(f(num))
	return count(b, pattern)
}

func Calc(num int, pattern string) int64 {
	if num > 100 || num < 0 {
		return -1
	}
	a1, a2, d := calcend(pattern)
	j := num - d
	switch {
	case j < 0:
		return 0
	case j == 0:
		return a2
	case j == 1:
		return a1
	}
	c1, c2 := calcmid(pattern)
	k1, k2 := int64(0), int64(0)
	for i := 1; i < j; i++ {
		if d%2 == i%2 {
			k1 += lm[i-1]
		} else {
			k2 += lm[i-1]
		}
	}
	// fmt.Printf("c1=%d,c2=%d,k1=%d,k2=%d,lm1=%d,lm2=%d\n", c1, c2, k1, k2, lm[j-1], lm[j-2])
	return (lm[j-1]*a1 + lm[j-2]*a2 + k1*c1 + k2*c2)
}

func main() {
	for i := 10; i < 97; i++ {
		fmt.Printf("%d: %v\n", i, Calc(i, "10110101101101"))
	}
}
